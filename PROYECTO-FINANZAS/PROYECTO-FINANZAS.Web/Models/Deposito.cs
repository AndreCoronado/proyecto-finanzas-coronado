//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PROYECTO_FINANZAS.Web.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Deposito
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Deposito()
        {
            this.Tasa_X_Deposito = new HashSet<Tasa_X_Deposito>();
        }
    
        public int DepositoId { get; set; }
        public decimal MontoDepositado { get; set; }
        public int NroDiasPlazoFijo { get; set; }
        public System.DateTime FechaDeposito { get; set; }
        public Nullable<System.DateTime> FechaCancelacion { get; set; }
        public Nullable<System.DateTime> FechaVencimiento { get; set; }
        public decimal Penalizacion { get; set; }
        public int CuentaCorrienteId { get; set; }
        public int TasaId { get; set; }
    
        public virtual CuentaCorriente CuentaCorriente { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tasa_X_Deposito> Tasa_X_Deposito { get; set; }
    }
}

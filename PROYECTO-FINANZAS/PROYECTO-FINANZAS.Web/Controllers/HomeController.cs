﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PROYECTO_FINANZAS.Web.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(String NombreUsuario, String Password)
        {
            var ObjUsuario = Context.Usuario.FirstOrDefault(X => X.NombreUsuario == NombreUsuario && X.Password == Password);

            if (ObjUsuario != null)
            {
                if (ObjUsuario.Bloqueado)
                {
                    TempData["Mensaje"] = "Problema de autorización: El usuario se encuentra bloqueado";
                    return View();
                }

                Session["UsuarioId"] = ObjUsuario.UsuarioId;
                Session["NombreUsuario"] = ObjUsuario.NombreUsuario;
                return RedirectToAction("Index", "Dashboard");
            }
            else
            {
                TempData["Mensaje"] = "Problema de autentificación: Usuario o Password incorrectos";
                return View();
            }
        }

        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("Login");
        }
    }
}